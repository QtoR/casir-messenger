# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from members.models import Member


@python_2_unicode_compatible
class Message(models.Model):
    """
        A message of the messenging app.
    """
    DURATION_CHOICES = [(1, "1s"),
                        (3, "3s"),
                        (10, "10s")]

    author = models.ForeignKey(Member,
                               verbose_name='Auteur',
                               related_name="author_of_messages")
    recipient = models.ForeignKey(Member,
                                  verbose_name='Destinataire',
                                  related_name="recipient_of_messages")
    content = models.TextField(verbose_name='Message')
    is_read = models.BooleanField(verbose_name='Message lu',
                                  default=False)
    duration = models.PositiveSmallIntegerField(verbose_name="Temps d'affichage",
                                                choices=DURATION_CHOICES,
                                                default=3)
    picture = models.FileField(verbose_name='Image',
                                upload_to="message_pictures/",
                                null=True,
                                blank=True)
    created_on = models.DateTimeField(verbose_name='Créé le',
                                      auto_now=True)

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'

    def __str__(self):
        return "Message from {} to {}".format(self.author, self.recipient)
