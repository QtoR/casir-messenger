# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(verbose_name='Content')),
                ('is_read', models.BooleanField(default=False, verbose_name='Message read')),
                ('duration', models.PositiveSmallIntegerField(default=3, verbose_name='Display duration', choices=[(1, '1s'), (3, '3s'), (10, '10s')])),
                ('picture', models.ImageField(upload_to='message_pictures/', null=True, verbose_name='Picture', blank=True)),
                ('created_on', models.DateTimeField(auto_now=True, verbose_name='Created on')),
                ('author', models.ForeignKey(related_name='author_of_messages', verbose_name='Author', to='members.Member')),
                ('recipient', models.ForeignKey(related_name='recipient_of_messages', verbose_name='Recipient', to='members.Member')),
            ],
            options={
                'verbose_name': 'Member',
                'verbose_name_plural': 'Members',
            },
            bases=(models.Model,),
        ),
    ]
