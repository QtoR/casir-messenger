# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.http import HttpResponse

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.dateformat import DateFormat

import os
import json

from members.models import Member
from members.forms import AddFriendForm
from members.forms import PendingRequestForm
from members.forms import NewMessageForm
from messaging.models import Message

@login_required
def index(request):
    current_member = request.user.member

    member_messages = Message.objects.filter(author=current_member.user.id).all()

    if request.method == "POST":
        if "add_friend_request" in request.POST:
            add_friend_form = AddFriendForm(member=current_member, data=request.POST)
            pending_request_form = PendingRequestForm(member=current_member)
            if add_friend_form.is_valid():
                for member in add_friend_form.cleaned_data["members"]:
                    member.friend_requests.add(current_member)
                add_friend_form = AddFriendForm(member=current_member)

                messages.success(request, "Demandes d'ajout effectuées avec succès")
        else:
            pending_request_form = PendingRequestForm(member=current_member, data=request.POST)
            add_friend_form = AddFriendForm(member=current_member)
            if pending_request_form.is_valid():
                if "refuse_pending_request" in request.POST:
                    for member in pending_request_form.cleaned_data["members"]:
                        current_member.friend_requests.remove(member)
                    messages.success(request, "Demandes en attente refusées avec succès")
                elif "accept_pending_request" in request.POST:
                    for member in pending_request_form.cleaned_data["members"]:
                        current_member.friend_requests.remove(member)
                        current_member.friends.add(member)
                    messages.success(request, "Demandes en attente acceptées avec succès")
            else:
                messages.warning(request, "Demandes en attente : opération annulée")
    elif request.method == "GET":
        add_friend_form = AddFriendForm(member=current_member)
        pending_request_form = PendingRequestForm(member=current_member)

    new_message_form = NewMessageForm(member=current_member)
    friends = current_member.friends.all()
    friend_requests = current_member.friend_requests.all()

    context = {'friends': friends,
               'friend_requests': friend_requests,
               'member_messages': member_messages,
               'add_friend_form': add_friend_form,
               'pending_request_form': pending_request_form,
               'new_message_form' : new_message_form}

    return render(request, "dashboard/index.html", context)


@login_required
def remove_friend(request, friend_pk):
    current_member = request.user.member
    removed_friend = get_object_or_404(Member, pk=friend_pk)
    current_member.friends.remove(removed_friend)
    messages.success(request, "Suppression effectuée avec succès")
    return redirect("dashboard:index")

@login_required
def get_message(request, message_pk):
    message = Message.objects.filter(id=message_pk).first()

    response = {
        'id'                  : message_pk,
        'author_id'           : message.author.user.id,
        'author_pseudonym'    : message.author.user.username,
        'recipient_id'        : message.recipient.user.id,
        'recipient_pseudonym' : message.recipient.user.username,
        'creation_date'       : DateFormat(message.created_on).format('d-m-Y'),
        'duration'            : message.duration,
        'content'             : message.content,
        'is_read'             : message.is_read,
        'image'               : None
    }

    if (message.picture != None):
        response['image'] = message.picture.url

    if message.picture != None:
        pass

    return HttpResponse(
        json.dumps(response),
        content_type='application/json'
    )

@login_required
def set_message_read(request, message_pk): 
    message = Message.objects.filter(id=message_pk).first()
    message.is_read = True
    message.save()

    return HttpResponse(
        json.dumps(True),
        content_type='application.json')