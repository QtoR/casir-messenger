# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals
from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = patterns('',
                       url(r'^index/$', 'dashboard.views.index', name="index"),
                       url(r'^remove/(?P<friend_pk>[\d]+)/$', 'dashboard.views.remove_friend', name="remove"),
                       url(r'^get_message/(?P<message_pk>[\d]+)/$', 'dashboard.views.get_message', name="get_message"),
                       url(r'^set_message_read/(?P<message_pk>[\d]+)/$', 'dashboard.views.set_message_read', name="set_message_read"),    
                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
